require './app'

require 'dotenv'
Dotenv.load

if ENV['STACKPROF'] == '1'
  puts 'enable stackprof'
  require 'stackprof'
  Dir.mkdir('/tmp/stackprof') unless File.exist?('/tmp/stackprof')
  use StackProf::Middleware, enabled: true,
    mode: :wall,
    interval: 500,
    save_every: 5,
    path: '/tmp/stackprof'
end

if ENV['SQLLOG'] == '1'
  puts 'enable sqllog'
  require 'mysql2/client/general_log'
  use Mysql2::Client::GeneralLog::Middleware, enabled: true, backtrace: true, path: '/tmp/general_log'
end

run App
