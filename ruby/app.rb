require 'erb'
require 'sinatra/base'
require './game'

class App < Sinatra::Base
  use Game

  configure do
    set :public_folder, File.expand_path('../../public', __FILE__)
  end

  configure :development do
    require 'sinatra/reloader'
    register Sinatra::Reloader
  end

  get '/initialize' do
    Game.initialize!
    204
  end

  get '/room/' do
    content_type :json
    { 'host' => next_server, 'path' => '/ws' }.to_json
  end

  get '/room/:room_name' do
    room_name = ERB::Util.url_encode(params[:room_name])
    path = "/ws/#{room_name}"

    content_type :json
    { 'host' => next_server, 'path' => path }.to_json
  end

  get '/' do
    send_file File.expand_path('index.html', settings.public_folder)
  end
  
  # wsのラウンドロビン用数値
  def next_server_count
    Thread.current[:hosts] ||= ENV.fetch('ISU_WEB_HOSTS') { 'app0071.isu7f.k0y.org' }.split(',')
  
    Thread.current[:server_count] ||= 0
    Thread.current[:server_count] = (Thread.current[:server_count] + 1) % Thread.current[:hosts].count
  end
  
  def next_server
    Thread.current[:hosts] ||= ENV.fetch('ISU_WEB_HOSTS') { 'app0071.isu7f.k0y.org' }.split(',')
    
    Thread.current[:hosts][next_server_count]
  end
end
